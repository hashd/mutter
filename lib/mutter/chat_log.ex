defmodule ChatLog do
  def start_link(opts \\ []) do
    {:ok, pid} = GenServer.start_link(ChatLog, %{ets_table_name: :chat_log_table, log_limit: 100}, opts)
  end

  def init(args) do
    %{ets_table_name: ets_table_name, log_limit: log_limit} = args

    :ets.new(ets_table_name, [:named_table, :set, :private])
    {:ok, %{log_limit: log_limit, ets_table_name: ets_table_name}}
  end

  def log_message(channel, message) do
    GenServer.call(:chat_log, {channel, message})
  end

  def get_logs(room) do
    GenServal.call(:chat_log, {room})
  end
end