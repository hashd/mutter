defmodule Mutter.Users do
  @default_name :mutter_users

  def start_link(name \\ @default_name) do
    Agent.start_link(fn -> [] end, name: name)
  end

  def get(name \\ @default_name) do
    Agent.get(name, &(&1))
  end

  def add_user(name \\ @default_name, user) when is_bitstring(user) do
    Agent.get_and_update(name, fn users -> [user | users] |> Tuple.duplicate(2) end)
  end

  def remove_user(name \\ @default_name, user) when is_bitstring(user) do
    Agent.update(name, fn users -> List.delete(users, user) end)
  end
end