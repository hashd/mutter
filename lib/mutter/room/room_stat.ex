defmodule Mutter.RoomStat do
  defstruct name: nil, about: nil, users: [], size: 0, created_at: nil, tags: []

  def new(room_name) do
    %__MODULE__{ name: room_name, created_at: get_current_time }
  end

  def start_link(name) do
    Agent.start_link(fn -> new(name) end)
  end

  def get(pid) do
    Agent.get(pid, &(&1))
  end

  def add_user(pid, user) when is_bitstring(user) do
    Agent.get_and_update(pid, fn state -> 
      %{ state | users: [user | state.users], size: state.size+1 } |> Tuple.duplicate(2)
    end)
  end

  def remove_user(pid, user) when is_bitstring(user) do
    Agent.get_and_update(pid, fn state ->
      %{ state | users: List.delete(state.users, user), size: state.size-1 } |> Tuple.duplicate(2)
    end)
  end

  defp get_current_time do
    {yy, mm, dd} = :erlang.date
    {h, m, s} = :erlang.time
    %{hours: h, minutes: m, seconds: s, day: dd, month: mm, year: yy}
  end

  @doc """
  Converts Mutter.RoomStat struct to a simple map.

  ### examples
  iex> %Mutter.RoomStat{users: ["bot"], size: 1} |> Mutter.RoomState.to_map
  %{users: ["bot"], size: 1}
  """
  def to_map(room_stat) do
    %{name: room_stat.name, about: room_stat.about, users: room_stat.users, size: room_stat.size, tags: room_stat.tags, created_at: room_stat.created_at}
  end
end