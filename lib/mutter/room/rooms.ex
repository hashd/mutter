defmodule Mutter.Rooms do
  alias Mutter.RoomStat, as: RoomStat

  @default_name :mutter_rooms

  def start_link do
    Agent.start_link(fn -> %{} end, name: @default_name)
  end

  def create_or_get_room(room) when is_bitstring(room) do
    room_pid = get(room)
    case room_pid do
      nil -> Agent.get_and_update(@default_name, fn rooms -> 
        {:ok, pid} = RoomStat.start_link(room)
        {pid, Map.put_new(rooms, room, pid)}
      end)
      _ -> room_pid 
    end
  end

  def get(room) when is_bitstring(room) do
    Agent.get(@default_name, fn rooms -> rooms[room] end)
  end

  def delete(room) when is_bitstring(room) do
    Agent.get_and_update(@default_name, fn rooms ->
      new_rooms = Map.delete(rooms, room)
      {new_rooms, new_rooms}
    end)
  end

  def list do
    Agent.get(@default_name, &(&1))
    |> Enum.filter(fn {k, _v} -> String.first(k) !== "_" end)
    |> Enum.map(fn {k, v} -> {k, v |> RoomStat.get} end)
    |> Enum.sort(fn ({_k1, v1}, {_k2, v2}) -> v1.size > v2.size end)
    |> Enum.map(fn {_k, v} -> RoomStat.to_map(v) end)
  end

  def add_user(room, user) when is_bitstring(room) and is_bitstring(user) do
    create_or_get_room(room)
    |> RoomStat.add_user(user)
  end

  def remove_user(room, user) when is_bitstring(room) and is_bitstring(user) do
    room_stat = get(room) |> RoomStat.remove_user(user)

    if room_stat.size === 0 do
      delete(room)
    end
  end
end