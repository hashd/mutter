# Mutter

### What's mutter
Mutter is the best chatting server, just kidding, it's probably the most lamest ever.

### Why mutter
Mutter is just a continuation of the standard chat app for Phoenix to emulate features of https://hack.chat

### Starting your mutter instance

To start Mutter application:

1. Install dependencies with `mix deps.get`
2. Start Phoenix endpoint with `mix phoenix.server`

Now you can visit `localhost:8080` from your browser.

### How does it work
Mutter provides channel/room support just like in IRC or hack.chat but it goes further by providing channel discovery for public rooms and also supports private rooms which are not listed in any board.

Any room whose name starts with an '_' character is considered to be a private room.

A room can have tags, an about message to help other people search for rooms easily.
