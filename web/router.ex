defmodule Mutter.Router do
  use Mutter.Web, :router

  socket "/ws", Mutter do
    channel "rooms:*", RoomChannel
  end

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
  end

  pipeline :api do
    plug :accepts, ["html", "json"]
  end

  scope "/", Mutter do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/:channel", PageController, :channel
  end

  scope "/api", Mutter do
    pipe_through :api

    get "/rooms", ApiController, :rooms
    get "/rooms/top", ApiController, :top_rooms
    get "/rooms/top/:number", ApiController, :top_rooms
    get "/rooms/new", ApiController, :new_rooms
    get "/rooms/new/:number", ApiController, :new_rooms
    get "/rooms/random", ApiController, :random_rooms
    get "/rooms/random/:number", ApiController, :random_rooms
  end
end
