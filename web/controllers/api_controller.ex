defmodule Mutter.ApiController do
  use Mutter.Web, :controller

  @default_result_size 10
  @fuzzy_match_threshold 0.7

  def rooms(conn, %{"search" => search_query}) do
    json conn, search_rooms(search_query)
  end

  def rooms(conn, _params) do
    json conn, %{ 
      top: top_rooms, 
      new: new_rooms, 
      random: random_rooms 
    }
  end

  def top_rooms(conn, %{"number" => num_string}) do
    json conn, top_rooms(num_string)
  end

  def top_rooms(conn, _params) do
    top_rooms(conn, %{"number" => @default_result_size})
  end

  def new_rooms(conn, %{"number" => num_string}) do
    json conn, new_rooms(num_string)
  end

  def new_rooms(conn, _params) do
    new_rooms(conn, %{"number" => @default_result_size})
  end

  def random_rooms(conn, %{"number" => num_string}) do
    json conn, random_rooms(num_string)
  end

  def random_rooms(conn, _params) do
    random_rooms(conn, %{"number" => @default_result_size})
  end

  #####################################
  # Private Functions
  #####################################
  defp top_rooms(number \\ @default_result_size)
  defp top_rooms(number) when is_integer(number) do
    Mutter.Rooms.list |> Enum.take(number)
  end

  defp top_rooms(number) when is_bitstring(number) do
    number |> String.to_integer |> top_rooms
  end

  defp new_rooms(number \\ @default_result_size)
  defp new_rooms(number) when is_integer(number) do
    Mutter.Rooms.list |> Enum.sort(fn (room1, room2) -> 
      r1_ct = room1.created_at
      r2_ct = room2.created_at
      (r1_ct.year > r2_ct.year) and (r1_ct.month > r2_ct.month) and (r1_ct.day > r2_ct.day) and (r1_ct.hours > r2_ct.hours) and (r1_ct.minutes > r2_ct.minutes) and (r1_ct.seconds > r2_ct.seconds)
    end) |> Enum.take(number)
  end

  defp new_rooms(number) when is_bitstring(number) do
    number |> String.to_integer |> new_rooms
  end

  defp random_rooms(number \\ @default_result_size)
  defp random_rooms(number) when is_integer(number) do
    Mutter.Rooms.list |> Enum.shuffle |> Enum.take(number)
  end

  defp random_rooms(number) when is_bitstring(number) do
    number |> String.to_integer |> random_rooms
  end

  defp search_rooms(search_query, result_size \\ @default_result_size) when is_bitstring(search_query) do
    Mutter.Rooms.list
    |> Enum.map(fn (room) -> { room, TheFuzz.Similarity.JaroWinkler.compare(room.name, search_query) } end)
    |> Enum.filter(fn ({_room, score}) -> score >= @fuzzy_match_threshold end)
    |> Enum.sort(fn ({_room1, score1}, {_room2, score2}) -> score1 >= score2 end)
    |> Enum.map(fn ({room, _score}) -> room end)
    |> Enum.take(result_size)
  end
end