defmodule Mutter.PageController do
  use Mutter.Web, :controller

  def index(conn, _params) do
    render conn, "home.html"
  end

  def channel(conn, %{"channel"=> channel}) do
    render conn, "index.html", channel: channel
  end
  def channel(conn, _params) do
    render conn, "home.html"
  end
end
