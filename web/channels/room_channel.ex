defmodule Mutter.RoomChannel do
  require IEx
  require Logger

  use Phoenix.Channel

  def join("rooms:" <> room, msg, socket) do
    user = msg["user"]

    Mutter.Rooms.add_user(room, user)
    Mutter.Users.add_user(user)
    Logger.log(:debug, "User: #{user} has joined the room: #{room}")

    socket = Map.put(socket, :assigns, Map.put(socket.assigns, :user, user))
    send self(), :after_join

    {:ok, socket}
  end

  def terminate(_reason, socket) do
    "rooms:" <> room = socket.topic
    user = socket.assigns.user

    Mutter.Rooms.remove_user(room, user)
    Mutter.Users.remove_user(user)
    Logger.log(:debug, "User: #{user} has left the room: #{room}")

    broadcast! socket, "current:users", (Mutter.Rooms.get(room) |> Mutter.RoomStat.get)
    broadcast! socket, "bye:user", %{user: user}

    {:noreply, socket}
  end

  def handle_info(:after_join, socket) do
    "rooms:" <> room = socket.topic
    user = socket.assigns.user

    broadcast! socket, "current:users", (Mutter.Rooms.get(room) |> Mutter.RoomStat.get)
    broadcast! socket, "new:user", %{user: user}

    {:noreply, socket}
  end

  def handle_in("new:message", msg, socket) do
    broadcast! socket, "new:message", %{user: msg["user"], body: msg["body"]}
    {:noreply, socket}
  end
end